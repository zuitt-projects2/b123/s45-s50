import App from './App';
import React from 'react';
import ReactDOM from 'react-dom';

/*
  ReactJS- syntax used is JSX.

  JSX - javascript + XML, It is an extension of Javascript that let's us create objects which will be compiled as HTML elements

  With JSX, we are able to create JS objects with HTML like syntax 
*/


let element = <h1> My First React App! </h1>
console.log(element)

/*
  ReactDOM.render(<reactElement>,<htmlElementSelectedById>)
*/
//with JSX
//let myName = <h2>123212</h2>
//without JSX
//let myName = ReactDOM.render('h2',{},"not created with JSX syntax")

let person = {
  name: "Monkey D. Luffy",
  age: 20,
  job: "Pirate Captain",
  income: 50000,
  expense: 30000
}

let sorcererSupreme = (
  <>
    <p>My name is {person.name}. I am {person.age} old. I work as a {person.job}.
        My income is {person.income}. My expense is {person.expense}. My Balance is {person.income - person.expense}.</p>
    <p>My name is {person.name}. I am {person.age} old. I work as a {person.job}.</p>
</>)

/*ReactJS does not like returning 2 elements unless they are wrapped by another element, or what we call a fragment <></>*/

ReactDOM.render(sorcererSupreme,document.getElementById('root'));
